public class Main {
    public static void main(String[] args) {
        CountMap<Integer> countMap = new CountMapImpl<Integer>();

        countMap.add(10);
        countMap.add(10);
        countMap.add(5);
        countMap.add(6);
        countMap.add(5);
        countMap.add(10);

        CountMap<Integer> countMap1 = new CountMapImpl<Integer>();

        countMap1.add(10);
        countMap1.add(10);
        countMap1.add(10254);
        countMap1.add(6);
        countMap1.add(5);
        countMap1.add(10);
        countMap1.add(12);
        countMap1.add(177);
        countMap.addAll(countMap1);


//        CountMap<String> countMap = new CountMapImpl<String>();
//
//        countMap.add("aaa");
//        countMap.add("bbb");
//        countMap.add("ccc");
//
//
//        CountMap<String> countMap1 = new CountMapImpl<String>();
//
//        countMap1.add("aaa");
//        countMap1.add("bbb");
//        countMap1.add("ccc");
//        countMap1.add("ddd");
//
//        countMap.addAll(countMap1);

        System.out.println(countMap.toString());
    }
}
