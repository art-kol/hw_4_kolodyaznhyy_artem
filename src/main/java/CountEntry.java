import java.util.Objects;

public class CountEntry<T>  {

    int counter;
    Object countObject;

    public CountEntry(T countObject) {

        this.counter = 1;
        this.countObject = countObject;
    }

    public CountEntry(T countObject, Integer counter) {
        this.counter = counter;
        this.countObject = countObject;
    }


    @Override
    public String toString() {
        return "CountEntry{" +
                "counter=" + counter +
                ", countObject=" + countObject +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountEntry<?> that = (CountEntry<?>) o;
        return counter == that.counter && Objects.equals(countObject, that.countObject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(counter, countObject);
    }
}
