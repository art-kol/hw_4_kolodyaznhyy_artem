import java.lang.reflect.ParameterizedType;
import java.util.*;

public class CountMapImpl<T> implements CountMap<T>{

    /**
     * elementData - контейнер
     * size - размер контейнера
     * pointer - курсор контейра
     */
    public T[] elementData;
    public int size;

    public int pointer = 0;

    private final Set<CountEntry<T>> containerSet = new HashSet<>();

    CountMapImpl(){
        this.size = 10;
        this.elementData = (T[]) new Object[size];
    }


    @Override
    public void add(T o) {

        try {
            elementData[pointer] = o;
            pointer++;
        } catch (ArrayIndexOutOfBoundsException e){
            T[] oldElementData = elementData;
            size = size*2;
            elementData = (T[]) new Object[size];
            for (int i = 0; i < oldElementData.length; i++) {
                elementData[i] = oldElementData[i];
            }
            elementData[pointer] = o;
            pointer++;
        }

    }

    @Override
    public int getCount(T o) {
        int count = 0;
        for (int i = 0; i < elementData.length; i++) {
            if(o == elementData[i]){
                count++;
            }
        }
        return count;
    }

    @Override
    public int remove(T o) {
        int removeCount = getCount(o);
        for (int i = 0; i < elementData.length; i++) {
            if(elementData[i] == o){
                elementData[i] = null;
            }
        }

        T[] oldElementData = elementData;
        elementData = (T[]) new Object[size];
        pointer = 0;
        for (int i = 0; i < oldElementData.length; i++) {
            if(oldElementData[i] != null){
                add(oldElementData[i]);
            }
        }
        return removeCount;
    }

    @Override
    public int size() {
        List<T> elemGetCountGreaterOne = new LinkedList<>();

        int diffElem = 0;
        for (int i = 0; i < pointer; i++) {
            if(getCount(elementData[i])==1){
                diffElem++;
            } else if (getCount(elementData[i])>1){
                if(!elemGetCountGreaterOne.contains(elementData[i])){
                    diffElem++;
                }
                elemGetCountGreaterOne.add(elementData[i]);
            }

        }

        return diffElem;
    }

    @Override
    public void addAll(CountMap<T> source) {
        T[] oldElementData = elementData;
        int oldPointer = pointer;
        elementData = (T[]) new Object[size];
        pointer = 0;
        CountMapImpl<T> newSource = (CountMapImpl<T>) source;
        List<T> elemToEntCollection = new LinkedList<>();

        for (int i = 0; i < newSource.pointer; i++) {
            System.out.println(oldElementData[i] +"  "+newSource.elementData[i]);
            if(oldElementData[i] == newSource.elementData[i]){
                add(sum(oldElementData[i],newSource.elementData[i]));
            } else {
                add(oldElementData[i]);
                elemToEntCollection.add(newSource.elementData[i]);
            }
        }

        pointer = oldPointer;
        for (int i = 0; i < elemToEntCollection.size(); i++) {
            add(elemToEntCollection.get(i));
        }
    }

    private T sum(T x, T y) {
        if (x == null || y == null) {
            return null;
        }

        if (x instanceof Double) {
            return (T) new Double(((Double) x).doubleValue() + ((Double) y).doubleValue());
        } else if (x instanceof Integer) {
            return (T) new Integer(((Integer) x).intValue() + ((Integer) y).intValue());
        } else if (x instanceof String) {
            return (T) new String(x.toString() + y.toString());
        } else {
            throw new IllegalArgumentException("Type " + x.getClass() + " is not supported by this method");
        }
    }


    @Override
    public Map toMap() {
        Map<T, Integer> uniqueValue = new HashMap<>();
        List<T> elemGetCountGreaterOne = new LinkedList<>();

        for (int i = 0; i < pointer; i++) {
            if(getCount(elementData[i])==1){
                uniqueValue.put(elementData[i], 1);
            } else if (getCount(elementData[i])>1){
                if(!elemGetCountGreaterOne.contains(elementData[i])){
                    uniqueValue.put(elementData[i], getCount(elementData[i]));
                }
                elemGetCountGreaterOne.add(elementData[i]);
            }

        }

        return uniqueValue;
    }

    @Override
    public void toMap(Map destination) {
        List<T> elemGetCountGreaterOne = new LinkedList<>();

        for (int i = 0; i < pointer; i++) {
            if(getCount(elementData[i])==1){
                containerSet.add(new CountEntry<T>(elementData[i]));
            } else if (getCount(elementData[i])>1){
                if(!elemGetCountGreaterOne.contains(elementData[i])){
                    containerSet.add(new CountEntry<T>(elementData[i], getCount(elementData[i])));
                }
                elemGetCountGreaterOne.add(elementData[i]);
            }

        }

    }



    @Override
    public String toString() {

        return "CountMapImpl{" +
                "elementData=" + Arrays.toString(elementData) +
                '}';
    }

    public T[] getElementData() {
        return elementData;
    }

    public int getSize() {
        return size;
    }

    public int getPointer() {
        return pointer;
    }
}
